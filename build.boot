(set-env!
 :source-paths    #{"src"}
 :resource-paths  #{"resources"}
 :dependencies '[[org.clojure/clojure "1.9.0"]

                 ;;ENV
                 [environ "1.0.1"]
                 [org.clojure/core.async "0.3.443"]

                 ;;DATA
                 [com.taoensso/encore "2.98.0"]
                 [org.clojure/core.cache "0.6.3"]
                 [org.clojure/core.memoize "0.5.6" :exclusions [org.clojure/core.cache]]
                 [net.mikera/core.matrix "0.60.3"]
                 ;;CLJS

                 [reagent "0.8.1"]
                 [mount "0.1.12"]
                 [re-frame "0.10.5"]
                 [datascript "0.15.5"] ;;Data store
                 [com.domkm/silk "0.1.1"]
                 [kibu/pushy "0.3.6"] ;;/urls
                 ;; [cljsjs/three "0.0.84-0"]
                 ;; [cljsjs/tween "16.3.1"]
                 [org.clojure/clojurescript "1.10.238"]
                 [day8.re-frame/async-flow-fx "0.0.11"]
                 [akiroz.re-frame/storage "0.1.2"]
                 [district0x/district-ui-component-form "0.1.11-SNAPSHOT"] ;;forms

                 ;;WEB
                 [ring "1.4.0"]
                 [ring/ring-defaults "0.1.5"]
                 [http-kit "2.2.0"]
                 [http-kit.fake "0.2.1"]
                 [com.taoensso/sente "1.11.0"]
                 [compojure                 "1.4.0"] ; Or routing lib of your choice
                 [hiccup                    "1.0.5"] ; Optional, just for HTML
                 [garden "1.3.0"]
                 [com.cemerick/friend "0.2.1"] ;;auth
                 [com.tombooth/friend-token "0.1.1-SNAPSHOT" :exclusions [clj-time]]

                 ;;FS
                 [me.raynes/fs "1.4.6"]
                 [me.raynes/conch "0.8.0"]

                 ;; Transit deps optional; may be used to aid perf. of larger data payloads
                 ;; (see reference example for details):

                 ;;TRANSPORT
                 [com.cognitect/transit-clj  "0.8.313"]
                 [com.cognitect/transit-cljs "0.8.256"]
                 [cljs-http "0.1.45"]

                 ;;MISC
                 [mvxcvi/puget "1.0.0"]
                 [com.taoensso/timbre "4.10.0"]
                 [com.taoensso/tufte "2.0.1"]

                 ;;CRYPTO
                 [digest "1.4.8"]
                 [district0x.re-frame/web3-fx "1.0.5"]
                 [district0x/bignumber "1.0.1"]
                 [district0x/cljs-solidity-sha3 "1.0.0"]
                 [district0x/district-cljs-utils "1.0.3"]
                 [district0x/district-encryption "1.0.0"]
                 [district0x/district-format "1.0.0"]
                 [district0x/district-graphql-utils "1.0.5"]
                 [district0x/district-sendgrid "1.0.0"]
                 [district0x/district-ui-component-active-account "1.0.0"]
                 [district0x/district-ui-component-active-account-balance "1.0.0"]
                 [district0x/district-ui-component-notification "1.0.0"]
                 [district0x/district-ui-graphql "1.0.0"]
                 [district0x/district-ui-logging "1.0.0"]
                 [district0x/district-ui-notification "1.0.1"]
                 [district0x/district-ui-now "1.0.1"]
                 [district0x/district-ui-reagent-render "1.0.1"]
                 [district0x/district-ui-router "1.0.3"]
                 [district0x/district-ui-router-google-analytics "1.0.0"]
                 [district0x/district-ui-smart-contracts "1.0.6"]
                 [district0x/district-ui-web3 "1.0.1"]
                 [district0x/district-ui-web3-account-balances "1.0.2"]
                 [district0x/district-ui-web3-accounts "1.0.5"]
                 [district0x/district-ui-web3-balances "1.0.2"]
                 [district0x/district-ui-web3-tx "1.0.8"]
                 [district0x/district-ui-web3-tx-id "1.0.1"]
                 [district0x/district-ui-web3-tx-log "1.0.2"]
                 [district0x/district-ui-window-size "1.0.1"]
                 [district0x/district-web3-utils "1.0.2"]


                 ;;DEV
                 [samestep/boot-refresh "0.1.0" :scope "test"]
                 [adzerk/boot-cljs          "1.7.228-2"  :scope "test"]
                 [adzerk/boot-cljs-repl     "0.3.3"      :scope "test"]
                 [adzerk/boot-reload        "0.4.13"      :scope "test"]
                 [pandeiro/boot-http        "0.8.3"      :scope "test"]
                 [com.cemerick/piggieback   "0.2.1"      :scope "test"]
                 [org.clojure/tools.nrepl   "0.2.12"     :scope "test"]
                 [weasel                    "0.7.0"      :scope "test"]
                 [crisptrutski/boot-cljs-test "0.3.0" :scope "test"]
                 [org.martinklepsch/boot-garden "1.3.2-0" :scope "test"]
                 [binaryage/dirac "1.1.3" :scope "test"]
                 [powerlaces/boot-cljs-devtools "0.2.0" :scope "test"]
                 [binaryage/devtools "0.9.4"]
                 ])

(require
 '[insieme.server.core]
 '[insieme.boot.tasks.compile-solidity :refer [compile-solidity]]
 '[samestep.boot-refresh :refer [refresh]]
 '[adzerk.boot-cljs      :refer [cljs]]
 '[adzerk.boot-cljs-repl :refer [cljs-repl start-repl]]
 '[adzerk.boot-reload    :refer [reload]]
 '[pandeiro.boot-http    :refer [serve]]
 '[crisptrutski.boot-cljs-test :refer [test-cljs]]
 '[org.martinklepsch.boot-garden :refer [garden]]
 '[powerlaces.boot-cljs-devtools :refer [cljs-devtools dirac]]
 )


(deftask build []
  (comp (speak)
     (cljs)
     (compile-solidity)
     (garden :styles-var 'insieme.styles.styles/screen
             :output-to "public/css/styles.css")))

(deftask run-sente-server
  "Runs sente http-kit webserver"
  []
  (fn [next-task]
    (fn [fileset]
      (insieme.server.core/start!)
      (next-task fileset))))

(deftask run []
  (comp
   ;; (serve :httpkit true
   ;;        :reload true
   ;;        :handler 'bonfire.api.core/app-ring-handler)
   (run-sente-server)
   (watch)
   (refresh)
   (cljs-repl)
   (reload)
   (build)))

;; (deftask run []
;;   (comp (serve)
;;      (watch)
;;      (cljs-repl)
;;      (dirac)
;;      (reload)
;;      (build)))


(deftask development []
  (task-options!
   compile-solidity {:directory "public/contracts"
                     :template "development.edn"}
   cljs {:optimizations :none
         :compiler-options
         {:closure-defines {'insieme.client.config.env.env "development"}}}
   reload {:on-jsload 'insieme.client.app/init
           :asset-path "public"})
  identity)


(deftask production []
  (task-options!
   compile-solidity {:directory "public/contracts"
                     :template "production.edn"}
   cljs {:optimizations :advanced
         :compiler-options
         {:closure-defines {'insieme.client.config.env.env "production"}}}
   garden {:pretty-print false})
  identity)


(deftask dev
  "Simple alias to run application in development mode"
  []
  (comp (development)
     (run)))


(deftask testing []
  (set-env! :source-paths #(conj % "test/cljs"))
  identity)

;;; This prevents a name collision WARNING between the test task and
;;; clojure.core/test, a function that nobody really uses or cares
;;; about.
(ns-unmap 'boot.user 'test)

(deftask test []
  (comp (testing)
     (test-cljs :js-env :phantom
                :exit?  true)))

(deftask auto-test []
  (comp (testing)
     (watch)
     (test-cljs :js-env :phantom)))
