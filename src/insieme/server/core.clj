(ns insieme.server.core
  (:require
   [insieme.server.services.config]
   [insieme.server.services.sente]
   [insieme.server.services.web]
   [mount.core :as mount]
   )
  (:gen-class))

;;;; Init

(defn stop! []
  (mount/stop))

(defn start! []
  (mount/start))

(defn -main [& args]
  (start!))
