(ns insieme.boot.tasks.compile-solidity
  {:boot/export-tasks true}
  (:require [boot.core :as c]
            [boot.util :as util]
            [clojure.edn :as edn]
            [me.raynes.fs :as fs]
            [me.raynes.conch :refer [programs with-programs let-programs] :as sh]
            ;; [cljstache.core :refer [render]]
            [puget.printer :as puget ]
            [clojure.data.json :as json]
            ;; [clj-yaml.core :as cyaml]
            [clojure.java.io :as io]
            [digest :as digest]
            [clojure.core.async :refer [<!!]]
            [clojure.string :as s]))

(defn compile-sol! [mode in out]
  (puget/pprint [:COMPILE mode in out])
  (let-programs [solc "solc"]
    (solc "--overwrite" "--optimize" "--bin" "--abi" in "-o" out)))

(defn- subs-ext
  [path ext]
  (.replaceAll path "\\.[a-zA-Z0-9_]+$" (str "." ext)))


(defn files-index [dir]
  (let [files (fs/list-dir dir)]
    (reduce (fn [fd f]
              (let [n (s/replace (:name f)
                                 (:extension f) "")]
                (if (or (= (:extension f)
                           ".bin")
                        (= (:extension f)
                           ".abi"))
                  (let [fd (if (get fd n)
                             fd
                             (assoc fd n {}))]
                    (assoc-in fd [n (keyword (s/replace (:extension f) "." ""))] f))
                  fd)))
            {}
            (map (fn [f]
                   (let [ext (fs/extension f)
                         n (.getName f)
                         dig (digest/sha-256 f)]
                     {:extension ext
                      :name n
                      :body (slurp f)
                      :digest dig}))
                 files))))

(c/deftask compile-solidity
  "Compiles solidity"
  [d directory VAL str "Directory to output"
   t template VAL str "Template file"
   m mode VAL [kw] "Mode"]
  (let [tmp-dir (c/tmp-dir!)]
    (puget/pprint [:DIR directory])
    (puget/pprint [:CONTRACT (str directory "/" template)])
    (fn middleware [next-handler]
      (c/empty-dir! tmp-dir)
      (fn handler [fileset]
        (let [in-files (c/input-files fileset)
              lc-files (c/by-ext [".sol"] in-files)
              template-files (c/by-path [(str directory "/" template)]
                                        in-files)
              _ (puget/pprint [:TFILES template-files])
              template-file (c/tmp-file (first template-files))
              template (edn/read-string (slurp template-file))
              index-path (str tmp-dir "/" directory "/idx-" (.getName template-file))]
          (doseq [in lc-files]
            (let [in-file  (c/tmp-file in)
                  in-path  (c/tmp-path in)]
              (puget/pprint [:IN in-path])
              (compile-sol! mode (.getAbsolutePath in-file) (str tmp-dir "/" directory))
              (puget/pprint [:RESULT (files-index (str tmp-dir "/" directory))])))
          ;; (spit index-path
          ;;       (prn-str template))
          (puget/pprint [:subt
                         (files-index (str tmp-dir "/" directory))
                         (keys template)])
          (spit (str index-path) ;;tmp-dir "/" directory "/index.edn"
                (prn-str (select-keys
                          (files-index (str tmp-dir "/" directory))
                          (keys template))))
          (-> fileset
              (c/rm lc-files)
              (c/add-resource tmp-dir)
              c/commit!
              next-handler))))))
