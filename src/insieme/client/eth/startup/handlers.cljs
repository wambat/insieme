(ns insieme.client.eth.startup.handlers
  (:require
   [taoensso.sente  :as sente  :refer (cb-success?)]
   [taoensso.timbre :refer-macros [log trace debug info warn error]]
   [re-frame.core :as re-frame :refer [reg-event-fx console dispatch trim-v reg-sub subscribe]]
   [district.ui.smart-contracts.deploy-events :as deploy-events]
   [taoensso.timbre :as timbre
    :refer-macros [log
                   trace
                   debug
                   info
                   warn
                   error
                   fatal
                   report]]
   [district0x.re-frame.web3-fx]
   [cljs-web3.core :as web3]
   [cljs-web3.db :as web3-db]
   [cljs-web3.eth :as web3-eth]
   [cljs-web3.evm :as web3-evm]
   [cljs-web3.net :as web3-net]
   [cljs-web3.personal :as web3-personal]
   [cljs-web3.shh :as web3-shh]
   [day8.re-frame.async-flow-fx]
   [akiroz.re-frame.storage :refer [persist-db]]
   [insieme.client.eth.startup.subs :as dsub]
   [mount.core :as mount]
   [district.ui.web3-accounts]
   [district.ui.smart-contracts]
   [district.ui.smart-contracts.events :as events]
   [district.ui.web3-accounts.subs :as accounts-subs]
   ))

(def web3-url "http://localhost:8549/")
;; (def web3 (web3/create-web3 web3-url))
(def gas-limit 5500000)
(def interceptors [(persist-db :insieme-app :persistent)])

#_(defn boot-flow
  []
  {:first-dispatch [::load-accounts {:on-ok ::accounts-loaded}]
   :rules [{:when :seen? :events ::accounts-loaded  :dispatch [::deploy-new-contracts {:on-ok ::contracts-deployed}]}
           ]})

(defn filter-new-contracts [to-deploy old-contracts]
  (remove (fn [[n c]]
            ;; (info [:WL
            ;;        (get-in old-contracts [n :bin :digest])
            ;;        (get-in c [:bin :digest])])
            (when-let [digest (get-in old-contracts [n :bin :digest])]
              (= digest (get-in c [:bin :digest]))))
          (into [] to-deploy)))

(defn register! []
  #_(reg-event-fx
   ::boot
   interceptors
   (fn [cofx [_ contracts]]
     {:db {:contracts-to-deploy contracts
           :contracts {}}
      :dispatch [::deploy-new-contracts]}))

  #_(reg-event-fx
   ::load-accounts
   interceptors
   (fn [{:keys [:db]} [_ {:keys [:on-ok :on-error]}]]
     {:db db
      :web3/call {:web3 web3
                  :fns [{:fn web3-eth/accounts
                         :on-success [(or on-ok ::accounts-loaded)]
                         :on-error [(or on-error ::error)]}]}}))
  ;; (reg-event-fx
  ;;  ::accounts-loaded
  ;;  interceptors
  ;;  (fn [{:keys [:db]} [_ accounts]]
  ;;    (info "ACCOUNTS-LOADED" accounts)
  ;;    {:db (assoc db :accounts accounts)}))

  (defn boot [contractsmap]
   (let [contracts (into [] contractsmap)
         existing-contracts @(subscribe [::dsub/contracts])
         to-deploy (into {}
                         (filter-new-contracts
                          contracts
                          existing-contracts))
         contract-defs (apply merge
                              (map (fn [[n c]]
                                     {(keyword n) {:abi (js/JSON.parse
                                                         (get-in c [:abi :body]))
                                                   :disable-loading-at-start? (contains? to-deploy n)
                                                   :bin (get-in c [:bin :body])}})
                                   contracts))]

     (info [:BOOTIN contract-defs])
     (mount/stop)
     (-> (mount/with-args
           {:web3 web3-url
            :web3-accounts {:polling-interval-ms 5000}
            :smart-contracts {;;:disable-loading-at-start? true
                              :contracts contract-defs}})
         (mount/start))
     (let [deploy-calls (map (fn [[n c]]
                               #_[::deploy-contract
                                  {:on-ok on-ok}
                                  (clj->js (js/JSON.parse
                                            (get-in c [:abi :body])))
                                  {:data (get-in c [:bin :body])
                                   :gas gas-limit
                                   :from (first (:accounts db))}
                                  [n c]]
                               
                               )
                             to-deploy)]
       ;; (info ["OLD CONTRACTS" existing-contracts])
       ;; (info ["TODEP CONTRACTS" to-deploy])
       ;; (info ["DB" db])
       (info ["DEPLOYING CONTRACTS" (map (comp keyword first ) to-deploy)])
       (dispatch [::deploy-contracts (map (comp keyword first ) to-deploy)]))))

  (reg-event-fx
   ::deploy-contracts
   interceptors
   (fn [{:keys [db]} [_ cs]]
     (let [calls (map
                  (fn [n]
                    [::deploy-events/deploy-contract (keyword n)
                     {:gas 4500000
                      :arguments []
                      :from @(subscribe [::accounts-subs/active-account])
                      ;; :on-success [::optional-callback]
                      ;; :on-error [::optional-error-callback]
                      }])
                  cs)]
       (info [:DB db])
       (info [:DC calls])
       {:dispatch-n calls})))

  (reg-event-fx
   ::contracts-deployed
   interceptors
   (fn [{:keys [:db]} [_ [n c] contract-instance]]
     (info [:CONTRACT-DEPLOYED contract-instance
            (aget contract-instance "address")
            n
            c])
     (when (aget contract-instance "address")
       {:db (-> db
                (assoc-in [:persistent :contracts n] {:address (aget contract-instance "address")
                                                      :bin {:digest (get-in c [:bin :digest] c)}
                                                      ;; :instance contract-instance
                                                      })
                (assoc-in [:contracts n] {}))
        ;; :dispatch [::contract-deployed contract-instance]
        })))

  (reg-event-fx
   ::contract-deployed
   interceptors
   (fn [{:keys [:db]} [_ contract-instance]]
     (info [:DEPLOYED_CONTRACT contract-instance])
     {:db (assoc-in db [:persistent :contract] contract-instance)})))

(reg-event-fx
 ::error
 (fn [coefx a]
   (info "ERROR" a)
   {}))
