(ns insieme.client.eth.startup.subs
  (:require
   [taoensso.timbre :refer-macros [log trace debug info warn error]]
   [re-frame.core :as re-frame :refer [reg-event-fx console dispatch trim-v reg-sub subscribe]]))

(defn accounts [db]
  (:accounts db))

(reg-sub
 ::accounts
 accounts)

(defn contracts [db]
  (or
   (get-in db [:persistent :contracts])
   {}))

(reg-sub
 ::contracts
 contracts)
