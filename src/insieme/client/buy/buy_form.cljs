(ns insieme.client.buy.buy-form
  (:require
   [district.ui.component.form.input :as inputs]
   [re-frame.core :as re-frame]
   [reagent.core :as r]
   [reagent.ratom :as ratom]))

(defn buy-form []
  (let [form-data (r/atom {})
        errors (r/atom {})]
    (fn []
      [inputs/with-label "Amount "
       [:div
        [inputs/amount-input {:form-data form-data
                              :id :amount
                              :placeholder "INCOINS"
                              :errors errors}]
        [inputs/pending-button
         {;;:pending? @tx-pending?
          ;; :disabled (or (-> @errors :local :vote/amount-for empty? not)
          ;;               @tx-success?)
          :pending-text "Voting..."
          :on-click #(re-frame/dispatch [:contract/call {;;:send-tx/id tx-id
                                                ;; :reg-entry/address (:reg-entry/address meme)
                                                :vote/option :vote.option/vote-for
                                                ;; :vote/amount (-> @form-data :vote/amount-for js/parseInt)
                                                }])}
         "Buy INCOINS"]]]))
  )
