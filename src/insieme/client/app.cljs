(ns insieme.client.app
  (:require [reagent.core :as reagent :refer [atom]]
            [taoensso.timbre :as timbre
             :refer-macros [log
                            trace
                            debug
                            info
                            warn
                            error
                            fatal
                            report]]
            [cljs-http.client :as http]
            [cljs.tools.reader.edn :as edn]
            [cljs.core.async :refer [<!]]
            [insieme.client.eth.startup.handlers :as startup]
            [insieme.client.eth.startup.subs :as startupsub]
            [insieme.client.buy.buy-form :as bform]
            [re-frame.core :as re-frame]
            [district.ui.smart-contracts.subs :as csubs]

            )
  [:require-macros
   [cljs.core.async.macros :refer [go]]])


(defn register-handlers! []
  (startup/register!))

(defn root-component []
  (let [contracts (re-frame/subscribe [::csubs/contracts])]
    (fn []
      [:div "INSIEME"
       [:div
        (if contracts
          (str @contracts)
          "NO CONTRACTS")]
       [bform/buy-form]])))

(defn contracts-to-update [old new]
  (reduce (fn [acc [name fd]]
            ;; (info [:NAME name])
            ;; (info [:FD fd])
            ;; (info [:CMP (get-in old [name :bin :digest]) (get-in fd [:bin :digest])])
            (if (and
                 (get fd :bin)
                 (not= (get-in old [name :bin :digest])
                       (get-in fd [:bin :digest])))
              (assoc acc name fd)
              acc))
          {}
          (into [] new)))

(defn redeploy! []
  (go (let [response (<! (http/get (str "contracts/idx-development.edn?r=" (rand-int 1000000))
                                   {:with-credentials? false}))
            body (:body response)]
        (info [:STATUS (:status response)])
        ;; (info [:RESP body])
        (if (pos? (count body))
          (startup/boot body)))))

(defn init []
  (info "INIT")
  (register-handlers!)
  (redeploy!)
  (reagent/render-component [root-component]
                            (.getElementById js/document "app")))


